#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

class NumberGuess {
public:
    NumberGuess() {
        top = 20;
        bottom = 1;
    }

    void setBottom(int x) {
        bottom = x;
    }

    void setTop(int x) {
        top = x;
    }

    void run() {
        int guess = 0;
        int tries = 0;
        bool running = true;
        number = bottom + rand() % top;

        while(running) {
            cout << "Making guess #" << ++tries << "-> ";
            cin >> guess;
            running = (guess != number);
            if (guess > number)
                cout << "value is less\n";
            else if (guess < number)
                cout << "value is greater\n";
        }

        cout << "you guessed for " << tries << " times\n";
    }

private:
    int bottom;
    int top;
    int number;
};

int main(int argc, char** argv) {
    srand(time(NULL));
    NumberGuess guess;
    guess.run();
    return 0;
}
